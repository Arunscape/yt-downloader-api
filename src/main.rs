#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

// #[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;

use rocket_contrib::json::{Json, JsonValue};
#[derive(Serialize, Deserialize)]
struct Video {
    url: String
}

use std::process::Command;
extern crate serde;
extern crate serde_json;


#[post("/get_info", format="json", data="<video>")]
fn get_info(video: Json<Video>) -> JsonValue {
    let output = Command::new("youtube-dl")
        .arg("--dump-json")
        .arg(&video.url)
        .output()
        .expect("failed to get info");

    let string_json = String::from_utf8_lossy(&output.stdout);
    
    let return_value = serde_json::from_str(&string_json).unwrap();
    JsonValue(return_value)
}

use std::io;
use rocket::response::Stream;
use std::process::ChildStdout;
use std::process::Stdio;
use std::io::BufReader;

#[post("/download_video", data="<video>")]
fn download_video(video: Json<Video>) -> io::Result<Stream<ChildStdout>> {
    let mut child = Command::new("youtube-dl")
        .arg(&video.url)
        .arg("-o")
        .arg("-")
        .stdout(Stdio::piped())
        .spawn()
        .expect("REEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
    
    // Stream::chunked(io::stdin(), u64)
    let reader = BufReader::new(child.stdout);
    Stream::chunked(reader, 10)
}

fn main() {
    rocket::ignite().mount("/v1", routes![get_info, download_video]).launch();
}
