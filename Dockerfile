# build stage
FROM rustlang/rust:nightly as build-stage
WORKDIR /app
COPY . .
# RUN rustup default nightly
RUN cargo build --release
# RUN cargo build

# production stage
# FROM alpine as production-stage
# for glibc support
FROM frolvlad/alpine-glibc as production-stage
RUN apk add youtube-dl glibc
WORKDIR /app
COPY --from=build-stage /app/target/release/yt-downloader-api ./yt-downloader-api
# COPY --from=build-stage /app/target/debug/yt-downloader-api ./yt-downloader-api
EXPOSE 8000
ENTRYPOINT [ "./yt-downloader-api" ]

